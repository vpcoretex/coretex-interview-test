package com.gmail.TestCases;

import com.gmail.pageobjects.OTPVerificationPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.base.setup.TestBaseSetup;
import com.gmail.pageobjects.createAccountPage;
import com.gmail.pageobjects.enterPhoneNumberPage;

public class verifyMobilePasscode extends TestBaseSetup {
	
	private createAccountPage loginPageObject;
	private enterPhoneNumberPage passwordPageObject;
	private OTPVerificationPage verificationPageObject;
	private WebDriver driver;
	
	@BeforeClass
	public void setUp() {

		driver = getDriver();
	}
	
	@Test(description = "Test01 : This test will verify the failure of incorrect passcode")
	public void verifyInvalidPasscodeTest() throws InterruptedException {

		loginPageObject = new createAccountPage(driver);
		loginPageObject.enterUserDetail();
		loginPageObject.clickNextButton();

		passwordPageObject = new enterPhoneNumberPage(driver);
		passwordPageObject.typePhoneNumber();
		passwordPageObject.clickSubmitPassword();

		verificationPageObject = new OTPVerificationPage(driver);
		verificationPageObject.typeOTP();
		verificationPageObject.clickSubmitPassword();

		
	}
	
	
}
