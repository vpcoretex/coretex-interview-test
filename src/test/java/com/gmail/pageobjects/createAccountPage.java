package com.gmail.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

public class createAccountPage {
	
	protected WebDriver driver;
	public WebDriverWait wait;
	
	public createAccountPage(WebDriver driver) {

//		this.driver = driver;
		PageFactory.initElements(driver,this);
	}

	@FindBy (xpath = "/html/body/div[1]/div/div[2]/div[1]/div[1]/div/div") WebElement googleLogo;
	@FindBy (xpath = "/html/body/div[1]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div[2]/div/div/button/div[2]") WebElement signInText;

	@FindBy (xpath = "/html/body/div[1]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[1]/div[1]/div/div[1]/div/div[1]/input") WebElement First_name_textField;
	@FindBy (xpath = "/html/body/div[1]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div[1]/div[2]/div/div[1]/div/div[1]/input") WebElement Last_name_textField;
	@FindBy (xpath = "/html/body/div[1]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[2]/div[1]/div/div[1]/div/div[1]/input") WebElement username_textField;
	@FindBy (xpath = "/html/body/div[1]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[3]/div[1]/div/div/div[1]/div/div[1]/div/div[1]/input") WebElement password_textField;
	@FindBy (xpath = "/html/body/div[1]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[3]/div[1]/div/div/div[2]/div/div[1]/div/div[1]/input") WebElement confirm_password_textField;

	@FindBy (xpath = "/html/body/div[1]/div[1]/div[2]/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div[1]/div/div/button") WebElement next_Button;


	public void enterUserDetail() {
		Reporter.log("from type username method");
		this.First_name_textField.sendKeys("Coretex");
		this.Last_name_textField.sendKeys("Tester");
		this.username_textField.sendKeys("coretex.testinterview");
		this.password_textField.sendKeys("@73remuraroad");
		this.confirm_password_textField.sendKeys("@73remuraroad");
	}
	
	public void verifyGoogleLogo() {
		if (googleLogo.isDisplayed() == true) {
			Reporter.log("Google logo is available");
		}
		else {
			Reporter.log("Google logo is not available");
		}
	}
	
	public void verifySignInText() {
		if (signInText.isDisplayed() == true) {
			Reporter.log("Sign in text available");
		}
		else {
			Reporter.log("Sign In Text not found");
		}
	}
	
	public void clickNextButton() {
		verifyGoogleLogo();
		verifySignInText();
		next_Button.click();
		//return new gmailPasswordPage(driver);
	}

}
