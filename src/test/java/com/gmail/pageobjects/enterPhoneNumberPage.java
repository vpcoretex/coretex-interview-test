package com.gmail.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;

public class enterPhoneNumberPage {
	
	private WebDriver driver;
	private WebDriverWait wait;

	@FindBy (xpath = "/html/body/div[1]/div/div[2]/div[1]/div[2]/div/div/div[2]/div/div[1]/div/form/span/section/div/div/div[2]/div/div[2]/div[1]/div/div[1]/input") WebElement phone_textField;
	@FindBy (xpath = "/html/body/div[1]/div/div[2]/div[1]/div[2]/div/div/div[2]/div/div[2]/div/div[1]/div/div/button/div[2]") WebElement next_Button;
	
	public enterPhoneNumberPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver,this);
	}

	public void waitForElementToLoad(WebDriver driver, int timeOutInSeconds, WebElement element) {
		Reporter.log("from wait element method");
		wait = new WebDriverWait(driver, timeOutInSeconds);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	
	public void typePhoneNumber() {
		Reporter.log("from type password before wait");
		waitForElementToLoad(driver, 20, phone_textField);
		this.phone_textField.sendKeys("0226666666");
		
	}
	
	public void clickSubmitPassword() {
		next_Button.click();
	}
}