package com.base.setup;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import io.github.bonigarcia.wdm.WebDriverManager;

import java.io.File;
import java.util.HashMap;

public class TestBaseSetup {

    public WebDriver driver;

    public WebDriver getDriver() {

        return driver;
    }

    private void setDriver() {

        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://accounts.google.com/signup/v2/webcreateaccount?hl=en&flowName=GlifWebSignIn&flowEntry=SignUp");

    }


    @BeforeClass
    public void initializeTestBaseSetup() {

        setDriver();
    }

    @AfterClass
    public void quitDriver() {

    	//driver.quit();
    }
}
